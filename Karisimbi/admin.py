from django.contrib import admin
from Karisimbi.models import Blog, Category, app_catalog



class BlogAdmin(admin.ModelAdmin):
    exclude = ['posted']
    prepopulated_fields = {'slug': ('title',)}


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

class app_catalogAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Blog)
admin.site.register(Category)
admin.site.register(app_catalog)

# Register your models here.


