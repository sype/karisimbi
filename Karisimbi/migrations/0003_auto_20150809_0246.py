# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Karisimbi', '0002_blog_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='app_catalog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('type', models.CharField(max_length=30)),
                ('version', models.CharField(max_length=30)),
                ('description', models.CharField(max_length=30)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RenameModel(
            old_name='sites',
            new_name='application',
        ),
    ]
