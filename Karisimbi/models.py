from django.db import models
from django.db.models import permalink
from django.contrib.auth.models import User
import datetime





class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateTimeField(default=datetime.datetime.now, db_index=True)
    category = models.CharField(max_length=100)



    def __unicode__(self):
        return '%s' % self.title



    @permalink
    def get_absolute_url(self):
        return ('view_blog_post', None, { 'slug': self.slug })

class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)

    def __unicode__(self):
        return '%s' % self.title

    @permalink
    def get_absolute_url(self):
        return ('view_blog_category', None, { 'slug': self.slug })







class application(models.Model):
    site_name = models.CharField(max_length=30)
    tld = models.CharField(max_length=5)
    version = models.CharField(max_length=15, default="latest")
    type = models.CharField(max_length=15)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    admin_user = models.CharField(max_length=15, default="admin")
    user = models.ForeignKey(User)
    status = models.BooleanField(max_length=30, default=False)

    def __unicode__(self):
        return self.site_name
    def get_absolute_url(self):
        return  ('create_application',None, { 'pk': self.pk })


class app_catalog(models.Model):
    name = models.CharField(max_length=30)
    type = models.CharField(max_length=30)
    version = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    #logo = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name







# Create your models here.
