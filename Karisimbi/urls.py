__author__ = 'pincemail'


from django.conf.urls import patterns, include, url
from Karisimbi.views import ListAll, ListCategory
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
    # Every users have to login

    #url(r'^profile/$', 'webadmin.views.home', name='home'),
    url(r'^$', ListAll.as_view(), name='ListAll'),
    url(r'^category/(?P<category>\w+)', ListCategory.as_view(), name='ListCategory'),

    )


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()