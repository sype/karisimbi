apache-libcloud==0.17.0
argcomplete==0.8.4
Django==1.7.5
pbr==0.10.7
psycopg2==2.6
requests==2.5.3
six==1.9.0
stevedore==1.2.0
