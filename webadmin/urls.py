from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from Karisimbi.views import  ListCategory
import Karisimbi.urls
from webadmin.views import ListCatalog, AppDetailView, AppCreateView
from django.views.generic.detail import DetailView
from django.utils import timezone

#from emailusernames.forms import EmailAuthenticationForm

admin.autodiscover()
urlpatterns = patterns('',
                       # Examples:
                       url(r'^login/', 'django.contrib.auth.views.login',{'template_name': 'registration/login3.html'}),
                       url(r'^accounts/profile/$', 'webadmin.views.home', name='home'),
                       url(r'^$',include(Karisimbi.urls)),
                       url(r'^category/(?P<category>\w+)', ListCategory.as_view(), name='ListCategory'),
                       url(r'^admin/', include(admin.site.urls)),
                       #url('^accounts/', include('django.contrib.auth.urls')),
                       url(r'^accounts/', include('registration.backends.default.urls')),
                       url(r'^accounts/catalog/$', ListCatalog.as_view(), name='ListCatalog'),
                       url(r'^accounts/catalog/(?P<pk>\d+)/$', AppDetailView.as_view(), name='application-detail'),
                       url(r'^accounts/create/(?P<pk>\d+)/$', AppCreateView.as_view(), name='create_application')


)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
