__author__ = 'pincemail'

import datetime
from django.http import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.views.generic import ListView,View,DetailView,CreateView
from Karisimbi.models import application, Blog, Category, app_catalog
from django.utils import  timezone
from django.http import request, HttpRequest






class ListCatalog(ListView):
    model = app_catalog
    template_name = 'backend_home.html'
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ListCatalog, self).dispatch(*args, **kwargs)
    def get_queryset(self):
        return app_catalog.objects.all()



@login_required()
def home(request):

    return  render_to_response('backend_home.html', context_instance=RequestContext(request))

class AppDetailView(DetailView):
    model = app_catalog
    template_name = 'detail_application.html'
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AppDetailView, self).dispatch(*args, **kwargs)
    def get_context_data( self, **kwargs):
        context = super(AppDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class AppCreateView(CreateView):
    model = application
    fields = ['site_name','admin_user','type','version']
    success_url = '/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(AppCreateView, self).form_valid(form)















