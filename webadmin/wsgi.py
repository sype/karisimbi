"""
WSGI config for webadmin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys, site

path = '/Users/sype/Lab/projects/karisimbi/karisimbi/webadmin'
if path not in sys.path:
    sys.path.append(path)
activate_this='/Users/sype/.virtualenvs/karisimbi/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webadmin.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
